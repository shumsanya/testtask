<?php
    try {
        $host = 'localhost';
        $db   = 'test_task';
        $user = 'root';
        $pass = 'root';
        $charset = 'utf8';

        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $pdo = new PDO($dsn, $user, $pass, $opt);
        //$conn = new PDO("mysql:host=localhost; dbname=amt_dealer", "root", "root");
    } catch (PDOException $err) {
        echo "Ошибка: не удается подключиться к базе данных: " . $err->getMessage();
    }