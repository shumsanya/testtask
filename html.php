<?php

function getPagination(){
    return  '
     <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">

            <li class="page-item ">
                <a class="page-link" href="<?php if($pageno <= 1){ echo "#"; } else { echo "?pageno=".($pageno - 1); } ?>" >Назад</a>
            </li>

            <li class="page-item"><a class="page-link" href="?pageno=1">1</a></li>

            <li class="page-item"><a class="page-link" href="<?php echo "?pageno=".($pageno + 1);?>">
                    <?php if($pageno == 1 || $pageno == 2 || $pageno == 3){
                        echo (2);
                    }else{
                        echo ($pageno);
                    }?>
                </a>
            </li>

            <li class="page-item">
                <a class="page-link" href="<?php
                    if($pageno == 1 || $pageno == 2 || $pageno == 3){
                        echo "?pageno=".(3);
                    }else{
                        echo "?pageno=".($pageno + 1);}?>"
                >
                    <?php if($pageno == 1 || $pageno == 2 || $pageno == 3){
                            echo (3);
                        }else{
                            echo ($pageno+1);
                        } ?>
                </a>
            </li>

            <li class="page-item">
                <a class="page-link" href="<?php echo "?pageno=".($pageno + 1);?>">Следующяя</a>
            </li>
        </ul>
    </nav>';

}


function get_form(){
    return '
   <form class="px-4 py-3" method="get" >

    <div class="form-group">
        <div class="form-check">
            <input type="radio" id="max_min" name="ege" value="max_min">
            <label class="form-check-label" for="max_min">
                отобрать от > до <
            </label>
        </div>
    </div>

    <div class="form-group">
        <div class="form-check">
            <input type="radio" id="min_max" name="ege" value="min_max" required>
            <label class="form-check-label" for="min_max">
                отобрать от < до >
            </label>
        </div>
    </div>

    <div class="form-group">
        <label for="min_ege">укажите минимальный возраст</label>
        <input type="date" class="form-control" name="min_ege" id="min_ege" placeholder="1" required>
    </div>

    <div class="form-group">
        <label for="max_ege">укажите максимальный возраст</label>
        <input type="date" class="form-control" name="max_ege" id="max_ege" placeholder="100 +" required>
    </div>

    <button type="submit" class="btn btn-primary">Сортировать</button>
    
</form>
    <div class="dropdown-divider"></div>
    <form class="px-4 py-3" method="get" >
     <div class="form-group">
        <label for="number_years">Введите число и узнаете кому исполнилось такое количество лет</label>
        <input type="text" class="form-control" name="number_years" id="number_years" placeholder="35"  >
    </div>


    <button type="submit" class="btn btn-primary">Сортировать</button>
    
</form>';
}