<?php

function get_all(){
    global $pdo;
    $sql = "SELECT * FROM `test`";
    $result = $pdo->query($sql);
    // Получаем результат
    return $data = $result->fetchAll();
}

function get_unique_names($column){
    global $pdo;

    $sql = "SELECT DISTINCT ".$column." FROM test";
    $stm = $pdo->prepare($sql);
    $stm->execute();
    return $data = $stm->fetchAll();
}

function get_sort_category($column_name, $field_name, $offset = 0, $size_page = 0){
    global $pdo;

    if ($size_page == 0){
        $sql = "SELECT category,firstname,lastname,email,gender,birthDate FROM test WHERE ".$column_name." = ?";
    }else{
        $sql = "SELECT * FROM test WHERE ".$column_name." = ?"." LIMIT ".$offset.", ".$size_page;
    }

    //$sql = "SELECT * FROM test WHERE 'category' = `films` LIMIT 1, 10";
    $stm = $pdo->prepare($sql);
    $stm->bindValue(1, $field_name);
    $stm->execute();
    return $data = $stm->fetchAll();
}

function get_sort_ege($ege, $min_ege, $max_ege, $offset = 0, $size_page = 0){
    global $pdo;

    if ($size_page == 0){
        if ($ege == 'max_min') {
            $sql = "SELECT category,firstname,lastname,email,gender,birthDate FROM test WHERE birthDate >= " . "'" . $min_ege . "'" . " and birthDate <= " . "'" . $max_ege . "'" . "ORDER BY birthDate DESC ";
        } else {
            $sql = "SELECT category,firstname,lastname,email,gender,birthDate FROM test WHERE birthDate >= " . "'" . $min_ege . "'" . " and birthDate <= " . "'" . $max_ege . "'" . "ORDER BY birthDate ";
            // $sql = "SELECT * FROM test WHERE birthDate >= '1970-10-10' and birthDate <= '1970-11-30' ORDER BY birthDate ";
        }
    }else {
        if ($ege == 'max_min') {
            $sql = "SELECT * FROM test WHERE birthDate >= " . "'" . $min_ege . "'" . " and birthDate <= " . "'" . $max_ege . "'" . "ORDER BY birthDate DESC LIMIT " . $offset . ", " . $size_page;
        } else {
            $sql = "SELECT * FROM test WHERE birthDate >= " . "'" . $min_ege . "'" . " and birthDate <= " . "'" . $max_ege . "'" . "ORDER BY birthDate LIMIT " . $offset . ", " . $size_page;
        }
    }

    $stm = $pdo->prepare($sql);
    $stm->execute();
    return $data = $stm->fetchAll(PDO::FETCH_ASSOC);

}

function get_number_years($number_years, $offset = 0, $size_page = 0){

    $today = new DateTime();
    $birthdate = get_birthdate($offset, $size_page);
    $result= [];
    foreach ($birthdate as $value){

        $interval = $today->diff(new DateTime($value['birthDate']));
        // echo $interval->format('%y  *');

        if ($interval->format('%y') == $number_years){
            $result[] = $value;
        }
    }
    return $result;
}

function get_birthdate($offset = 0, $size_page = 0){
    global $pdo;

    if ($size_page == 0){
        $sql = "SELECT category,firstname,lastname,email,gender,birthDate FROM `test` WHERE birthDate";
        $stm = $pdo->prepare($sql);
        $stm->execute();
        return $data = $stm->fetchAll();
    }else{
        $sql = "SELECT * FROM `test` WHERE birthDate LIMIT " . $offset . ", " . $size_page;
        $stm = $pdo->prepare($sql);
        $stm->execute();
        return $data = $stm->fetchAll();
    }
}






function get_pages($size_page, $offset){
    global $pdo;

    $stm = $pdo->prepare('SELECT * FROM `test` LIMIT ?, ?');
    $stm->bindValue(1, $offset, PDO::PARAM_INT);
    $stm->bindValue(2, $size_page, PDO::PARAM_INT);
    $stm->execute();
    return $data = $stm->fetchAll();
}







