<?php

require 'head.php';
require_once 'connectBD.php';
require_once 'sql_query.php';
require_once 'html.php';



    if($_FILES["file"]){

    $my_file = fopen($_FILES['file']['tmp_name'], "r");

    $name_table = $field = "`".str_replace("`","``",$_FILES['file']['name'])."`";
    try {
        $sql = "CREATE Table test(
                    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                    category VARCHAR(30) NOT NULL,
                    firstname VARCHAR(30) NOT NULL,
                    lastname VARCHAR(30) NOT NULL,
                    email VARCHAR(50),
                    gender VARCHAR(20) NOT NULL,
                    birthDate DATE NOT NULL
                 )";
        $pdo->exec($sql);
    }catch (PDOException $e){
        echo $sql . "<br>" . $e->getMessage();
    }

        $counter = 0; // Не пишем в таблицу название столбцов, пропуск первой строки
    while (($data = fgetcsv($my_file, 1000, ",")) !==false){

        if ($counter == 0){ $counter++; continue; }
        $sql = "INSERT INTO Test (category, firstname, lastname, email, gender, birthDate) VALUE (?,?,?,?,?,?)";
        $stmt= $pdo->prepare($sql);
        $stmt->execute([$data[0],$data[1],$data[2],$data[3],$data[4],$data[5]]);
    }

    fclose($my_file);
}

    // Задаем количество строк выводимых на одной странице
    $size_page = 10;

    // Начало выборки данных из таблици
    $offset=0;


 //* Пагинация *//

     // Поверка, есть ли GET запрос
     if (isset($_GET['pageno'])) {
         $pageno = $_GET['pageno'];
     } else {
         $pageno = 1;
     }

     // Вычисляем из какого места начать выводить
     $offset = (intval($pageno)-1) * $size_page;

     // выбираем как фильтровать, сортировать данные
     if (!$_GET["ege"] == ''){
         $mass_data = get_sort_ege($_GET['ege'], $_GET['min_ege'], $_GET['max_ege'], $offset, $size_page);
     }elseif($_GET['column_name']){
         $mass_data = get_sort_category($_GET['column_name'], $_GET['field'], $offset, $size_page);
     }elseif($_GET["number_years"]) {
         $result = get_number_years($_GET["number_years"]);
         $mass_data = array_slice($result, $offset, $size_page);
     }else{
        $result = get_all();
        $mass_data = array_slice($result, $offset, $size_page);
     }
?>

<div class="container" style="margin-top: 30px">
 <?php if ($_GET){?>
     <a href="table.php"><button class="btn btn-info">Показать без сортировки</button></a>
     <a href="<?php $str = "table_seve.php?field=". $_GET['field']. "& column_name=". $_GET['column_name']. "& ege=". $_GET['ege']. "& min_ege=". $_GET['min_ege']. "& max_ege=". $_GET['max_ege']. "& number_years=". $_GET['number_years']; echo $str?>">
         <button class="btn btn-info" style="color: #a71d2a; font-weight: bold">Сохранить данные таблици в файл</button>
     </a>
 <?php }else { ?>
    <button class="btn btn-info" style="color: #a71d2a">Сохранить данные таблици в файл</button> <!--field=Heller column_name=lastname ege= min_ege= max_ege=-->
   <?php } ?>
</div>

<div class="container" style="margin-top: 30px">
    <table class="table table-bordered table-dark">
      <thead >
        <tr class="table-info">
            <th scope="col" style="color: #005cbf">category
                <div class="btn-group dropright">
                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding: 0 3px;"></button>
                    <div class="dropdown-menu" >
                        <?php
                            $heading_name = get_unique_names('category');
                            foreach($heading_name as $value){  ?>
                              <a class="dropdown-item" href="?field=<?php echo ($value['category']); ?>& column_name=category"><?php echo ($value['category']); ?></a>
                            <?php } ?>
                    </div>
                </div>
            </th>
          <th scope="col" style="color: #005cbf">firstname
              <div class="btn-group dropright">
                  <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding: 0 3px;"></button>
                  <div class="dropdown-menu" >
                      <?php
                      $heading_name = get_unique_names('firstname');
                      foreach($heading_name as $value){  ?>
                          <a class="dropdown-item" href="?field=<?php echo ($value['firstname']); ?>& column_name=firstname"><?php echo ($value['firstname']); ?></a>
                      <?php } ?>
                  </div>
              </div>
          </th>
          <th scope="col" style="color: #005cbf">lastname
              <div class="btn-group dropright">
                  <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding: 0 3px;"></button>
                  <div class="dropdown-menu" >
                      <?php
                      $heading_name = get_unique_names('lastname');
                      foreach($heading_name as $value){  ?>
                          <a class="dropdown-item" href="?field=<?php echo ($value['lastname']); ?>& column_name=lastname"><?php echo ($value['lastname']); ?></a>
                      <?php } ?>
                  </div>
              </div>
          </th>
          <th scope="col" style="color: #005cbf">email</th>
          <th scope="col" style="color: #005cbf">gender
              <div class="btn-group dropright">
                  <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding: 0 3px;"></button>
                  <div class="dropdown-menu" >
                      <?php
                      $heading_name = get_unique_names('gender');
                      foreach($heading_name as $value){  ?>
                          <a class="dropdown-item" href="?field=<?php echo ($value['gender']); ?>& column_name=gender"><?php echo ($value['gender']); ?></a>
                      <?php } ?>
                  </div>
              </div>
          </th>
          <th scope="col" style="color: #005cbf">birthDate
              <div class="btn-group dropright">
                  <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding: 0 3px;"></button>
                  <div class="dropdown-menu" >
                      <?php echo get_form();?>
                  </div>
              </div>
          </th>
        </tr>
      </thead>
      <tbody>
      <?php
// Заполняем таблицу выбраными данными
       foreach ($mass_data as $item){ ?>
          <tr>
              <?php
                    echo ("<td>". $item['category']  ."</td>");
                    echo ("<td>". $item['firstname']  ."</td>");
                    echo ("<td>". $item['lastname']  ."</td>");
                    echo ("<td>". $item['email']  ."</td>");
                    echo ("<td>". $item['gender']  ."</td>");
                    echo ("<td>". $item['birthDate']  ."</td>");
              ?>
        </tr>
      <?php } ?>
      </tbody>
   </table>
</div>


<!--  ПАГИНАЦИЯ  -->
<nav aria-label="Page navigation example">
    <ul class="pagination justify-content-center">
<!-- Кнопка Назад -->
        <li class="page-item ">
            <a class="page-link" href="<?php if($pageno <= 1){ echo '#'; } else {$str = "?pageno=".($pageno - 1)."& field=". $_GET['field']. "& column_name=". $_GET['column_name']. "& ege=". $_GET['ege']. "& min_ege=". $_GET['min_ege']. "& max_ege=". $_GET['max_ege']. "& number_years=". $_GET['number_years']; echo $str;} ?>" >Назад</a>
        </li>

<!-- Кнопка 1 -->
        <li class="page-item"><a class="page-link" href="<?php $str = "?pageno=1& field=". $_GET['field']. "& column_name=". $_GET['column_name']. "& ege=". $_GET['ege']. "& min_ege=". $_GET['min_ege']. "& max_ege=". $_GET['max_ege']; echo $str;?>">1</a></li><!--filter=1&%20field=toys&%20column_name=category-->

<!-- Кнопка 2 -->
            <?php if($pageno == 1 || $pageno == 2 || $pageno == 3){
                $button_2 = 2;
            }else{
                $button_2 = $pageno;
            }?>
        <li class="page-item"><a class="page-link" href="<?php $str = "?pageno=".$button_2."& field=". $_GET['field']. "& column_name=". $_GET['column_name']. "& ege=". $_GET['ege']. "& min_ege=". $_GET['min_ege']. "& max_ege=". $_GET['max_ege']. "& number_years=". $_GET['number_years']; echo $str;?>">
                <?php echo $button_2; ?>
            </a>
        </li>

<!-- Кнопка 3 -->
            <?php if($pageno == 1 || $pageno == 2 || $pageno == 3){
                $button_3 = 3;
            }else{
                $button_3 = $pageno+1;
            } ?>
        <li class="page-item">
            <a class="page-link" href="<?php $str = "?pageno=".$button_3."& field=". $_GET['field']. "& column_name=". $_GET['column_name']. "& ege=". $_GET['ege']. "& min_ege=". $_GET['min_ege']. "& max_ege=". $_GET['max_ege']. "& number_years=". $_GET['number_years']; echo $str;?>">
                <?php echo $button_3; ?>
            </a>
        </li>

<!-- Кнопка Следующяя -->
        <li class="page-item">
            <a class="page-link" href="<?php $str = "?pageno=".($pageno + 1)."& field=". $_GET['field']. "& column_name=". $_GET['column_name']. "& ege=". $_GET['ege']. "& min_ege=". $_GET['min_ege']. "& max_ege=". $_GET['max_ege']. "& number_years=". $_GET['number_years']; echo $str;?>">Следующяя</a>
            <!--<a class="page-link" href="<?php /*echo "?pageno=".($pageno + 1);*/?>">Следующяя</a>-->
        </li>

    </ul>
</nav>

<!-- скрипти нужны для работы выпадающих элементов «построены» на сторонней библиотеке Popper.js-->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
